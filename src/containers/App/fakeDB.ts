import { User } from "./interfaces";

// voor makkelijker te werken met de dummy data heb ik de labels opgesplitst
// In User staat er dan bvb trainingId -> 0 -> flicking
// dit bespaart wat copy paste werk. (Normaal zit dit al in het object wanneer het van de API komt)

const categories = [
  'flicking', 'speed', 'strafe dodging', 'awareness', 'Target ACQ', 'Reaction', 'Tracking'
];

const subCategories = [
  'micro', 'macro', 'Allround'
]

const levelLabels = [
  '1', '2', '3', '4', '5', 'X'
]

const fakeUser: User = {
  trainingProgress: [
    {
      trainingId: 0,
      subTrainingProgress: [
        {
          subProgressId: 0,
          lastLevelId: 2,
          levelProgress: [
            {
              levelId: 0,
              highScore: 43930,
              distance: "long"
            },
            {
              levelId: 1,
              highScore: 4390,
              distance: "short"
            },
            {
              levelId: 2,
              highScore: 43880,
              distance: "short"
            },
          ],
        },
        {
          subProgressId: 1,
          lastLevelId: 1,
          levelProgress: [
            {
              levelId: 0,
              highScore: 43930,
              distance: "short"
            },
            {
              levelId: 1,
              highScore: 413230,
              distance: "medium"
            },
            {
              levelId: 2,
              highScore: 0,
              distance: "short"
            },
          ],
        },
        {
          subProgressId: 2,
          lastLevelId: 0,
          levelProgress: [
            {
              levelId: 0,
              highScore: 0,
              distance: "short"
            },
            {
              levelId: 1,
              highScore: 0,
              distance: "long"
            },
            {
              levelId: 2,
              highScore: 0,
              distance: "short"
            },
          ],
        }
      ]
    },
  ]
}

export {
  fakeUser,
  categories,
  subCategories,
  levelLabels,
}