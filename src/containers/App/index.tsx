import * as React from "react";

import Training from "../Training";
import "./styling/base.scss";
import Container from "react-bootstrap/Container";
import { fakeUser } from "./fakeDB";

const App = () => {
  return (
    <Container className="text-light">
      <Training
        user={fakeUser}
      />
    </Container>
  );
}

export default App