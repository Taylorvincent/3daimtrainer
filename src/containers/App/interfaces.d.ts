import { ButtonProps } from "react-bootstrap/Button";

interface User {
  trainingProgress: TrainingProgress[]
}

interface TrainingProgress {
  trainingId: number;
  subTrainingProgress: SubTrainingProgress[];
}

interface SubTrainingProgress {
  subProgressId: number;
  lastLevelId: number;
  levelProgress: LevelProgress[]
}

interface LevelProgress {
  levelId: number;
  distance: "long" | "medium" | "short"
  highScore: number | null;
}

export {
  User,
  TrainingProgress,
  SubTrainingProgress,
  LevelProgress
}