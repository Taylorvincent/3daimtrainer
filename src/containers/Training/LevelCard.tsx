import * as React from "react";
import { LevelProgress } from "../App/interfaces";
import { levelLabels } from "../App/fakeDB";
import styled from "styled-components";
import Button from "react-bootstrap/Button";

interface Props {
  level: LevelProgress
  isLastLevel: boolean
  isLocked: boolean
}

const LevelCard = (props: Props) => {
  return (
    <LvlCard>
      <Top {...props}>
        <div>
          <Label {...props}>Level</Label>
          <LevelText> {levelLabels[props.level.levelId]} </LevelText>
        </div>
        {!props.isLocked && 
          <React.Fragment>
            <div>
              <Label {...props}>Distance</Label>
              <div> {props.level.distance} </div>
            </div>
            <div>
              <Label {...props}>High Score</Label>
              <div> {props.level.highScore} </div>
            </div>
          </React.Fragment>
        }
      </Top>
      <Bottom>
        {!props.isLocked || props.isLastLevel ? 
        <React.Fragment>
          <Button variant="dark">
            A
          </Button>
            <Button className={props.isLastLevel ? "btn-last-level play" : "btn-dark play"}>
              play
            </Button>
        </React.Fragment>
        :
        <Locked>
          icon LOCKED
        </Locked>
      }
      </Bottom>
    </LvlCard>
  )
}

const LvlCard = styled.div`
  display: inline-flex;
  flex-direction: column;

  & > * {
    display: flex;
    justify-content: space-between;
    padding: 20px 40px;
  }

  & > *> * > *:nth-child(2){
    margin: auto;
    }
`;

const Top = styled.div<Props>`
  ${p => {
    if(p.isLastLevel){
      return "background-color: var(--primary);"
    } else if (p.isLocked) {
      return "background-color: var(--dark-blue);"
    } else {
      return "background-color: var(--dark-primary);"
    }
  }}

  & > * {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`;

const Bottom = styled.div`
  padding: 20px 40px;
  background-color: var(--darker-blue);
  height: 86px;
  align-items: center;
  .play{
    flex-grow: 1;
    margin-left: 10px;
  }
`;

const Label = styled.div<Props>`
  text-transform: uppercase;
  color: var(${p => p.isLastLevel ? "--lightgray-blue" : "--gray-blue" });
`;

const LevelText = styled.div`
  font-size: 4rem;
`;

const Locked = styled.div`
  color: var(--gray-blue);
`;

export default LevelCard