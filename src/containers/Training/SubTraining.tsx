import * as React from "react";
import { SubTrainingProgress } from "../App/interfaces";
import LevelCard from "./LevelCard";
import styled from "styled-components";

interface Props {
  name: string
  subTraining: SubTrainingProgress
}

const SubTraining = (props: Props) => {
  return(
    <Row>
      <h2>{props.name}</h2>
      <Slider>
        {props.subTraining.levelProgress.map(level => {
          return (
            <LevelCard 
              key={level.levelId} 
              level={level} 
              isLastLevel={level.levelId == props.subTraining.lastLevelId}
              isLocked={level.levelId > props.subTraining.lastLevelId}
            />
          )
        })}
      </Slider>
    </Row>
  )
}

export default SubTraining

const Slider = styled.div`
  display: grid;
  gap: 15px;
  grid-auto-columns: 360px;
  & > * {
    grid-row: 1;
  }
`;

const Row = styled.div`
  margin-top: 40px;
`;