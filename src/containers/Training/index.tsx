import * as React from "react";
import { User, TrainingProgress } from "../App/interfaces";
import SubTraining from "./SubTraining";
import Button from "react-bootstrap/Button";
import { categories, subCategories, levelLabels } from "../App/fakeDB";
import styled from "styled-components";

interface Props {
  user: User
}

const Training = (props: Props) => {
  const [activeTrainingId, setActiveTrainingId] = React.useState(0);
  const [activeTraining, setActiveTraining] = React.useState<TrainingProgress>();

  React.useEffect(() => {
    setActiveTraining(
      props.user.trainingProgress.find(
        training => training.trainingId == activeTrainingId
      )
    )
  }, [activeTrainingId])

  return (
    <div>
      <h1>Skill Training</h1>

      <ToolBar>
        <ButtonGroup>
          {categories.map((cat, i) => {
            return (
              <Button 
                key={i}
                onClick={() => setActiveTrainingId(i)}
                >
                  {cat}
              </Button>
            )
          })}
        </ButtonGroup>

        <ButtonGroup>
          <Button variant="dark">Game Settings</Button>
        </ButtonGroup>
      </ToolBar>

      <div>
        {activeTraining && 
          subCategories.map((subCat, i) => {
            return (
              <SubTraining 
                key={i}
                name={subCat}
                subTraining={activeTraining.subTrainingProgress.find(
                  subTraining => subTraining.subProgressId == i
                )}
              />
            )
          })
        }
      </div>
    </div>
  )
}

export default Training

const ToolBar = styled.div`
  display: flex;
  justify-content: space-between;
`;

const ButtonGroup = styled.div`
  display: grid;
  gap: 6px;

  & > * {
    grid-row: 1
  }
`;