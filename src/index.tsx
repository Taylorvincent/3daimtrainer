
// Import all the third party stuff
import * as React from 'react';
import * as ReactDOM from 'react-dom';


// Import root app
import App from './containers/App';



const MOUNT_NODE = document.getElementById('app');

ReactDOM.render(
  <App />,
  MOUNT_NODE,
);

