const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: {
    main: './src/index.tsx',
  },
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.tsx', '.scss', '.css']
  },
  devServer: {
    historyApiFallback: true,
  },
  module: {
    rules: [
    // entry: './src/index.js',
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      { 
        test: /\.tsx?$/, 
        loader: 'ts-loader',
        exclude: '/node_modules',
        resolve: {extensions: ['.js', '.jsx', '.react.js', '.ts', '.tsx', '.scss', '.css']} ,
      },
      {
        test: /\.scss$/,
        use: [{
          loader: MiniCssExtractPlugin.loader , // inject CSS to page
        }, {
          loader: 'css-loader', // translates CSS into CommonJS modules
        }, {
          loader: 'postcss-loader', // Run postcss actions
          options: {
            plugins: function () { // postcss plugins, can be exported to postcss.config.js
              return [
                require('autoprefixer')
              ];
            }
          }
        }, {
          loader: 'sass-loader' // compiles Sass to CSS
        }]
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    }),
    new MiniCssExtractPlugin(),
  ],
  optimization: {
       splitChunks: {
         chunks: 'all',
       },
     },
};